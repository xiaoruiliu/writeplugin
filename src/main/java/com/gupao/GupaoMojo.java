package com.gupao;


import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.util.List;


@Mojo(name = "gupaoedu",defaultPhase = LifecyclePhase.PACKAGE)
public class GupaoMojo extends AbstractMojo {
    @Parameter
    private String msg;//演示插件传参，设置参数
    @Parameter
    private List<String> list;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        System.out.println("gupao pluign test ！！！" + msg);
        System.out.println("gupao pluign test ！！！" + list);
    }

}
